# Elemental Hats. (Name Pending)
## Lore (Brief)
User is a wisp-like creature. The wisps can harness the natural elements to greatly enhance their abilities, 
but training is required initially to unlock the basic powers. 
The player starts in a training arena where they receive their formal training (read: tutorial), 
learn about the essential mechanics of the game, etc.

Hats are an element of the game, but more for endgame replayability rather than as part of the story. 
However, they could be included as part of the story e.g. "venturing on a journey to find and reclaim the Hats of the 
Ancestors, lost aeons ago to unknown beings"

## Basic Mechanics
Turn based RPG. Top Down 3D overworld. Side 3D Battle Screen. See [Mechanics Design](./MechanicsDesign/MechanicsDesign.md) for more information.

## Elements
A core aspect of the game will be based on different Elements. Each element will have its own specialty.
- Earth (Tank)
- Fire (DPS)
- Water (Buff)
- Electric (Chain Attacks)
- Light (Heal)
- Dark (Debuff)

Starting (Primary) Elements: Earth, Fire, Water

Augment (Secondary) Elements: Electric, Light, Dark. 

Augment Elements are intended to provide bonuses to existing Primary elements, creating unique combinations.

Each element will have a weakness and a strength:

Earth > Electric > Water > Fire > Earth

Light > Dark > Light (x2 Damage both ways)

## Skill Trees
It's an RPG after all.

#### Starting Neutral Skill Tree. Gives the base skills. E.g. "Shield"
- Attack - Basic attack, does damage.
- Shield - Basic defend, blocks damage.
    - When Neutral: Block's 2X damage
    - When Earth: Blocks 3X Damage. Extra block carries over?
    - When Water: Y% Chance to avoid all damage?
    - When Fire: Blocks X Damage, when hit deals Z% Damage?
- Special - Skill that does a vastly different thing depending on element. 
    - Neutral: Picks a Random element's special? - perhaps slight boost from current battlefield environment
    - Earth: Deals Damage, reduces enemies attack.
    - Water: Gives big buff. Attack? Dodge? Speed?
    - Fire: AoE Nuke. 'Nuff Said.
- Elemental Overload - Can only be performed with a certain criterion? - Maybe the support wisps from quests?
    - Choose 2 elements and combine their effects.
    - The first element will be the element you start in, with the second being the element you end in. 
    I.e. Water -> Fire = Buff then AoE. Fire -> Water = AoE then Buff.
- Elemental Adaptation (Passive) - This is the skill that lets you switch elements. 
It also applies the Elemental buffs to the skills above.

#### Elemental Trees
- Fire Tree: Augments skills in a largely offensive way.
- Water Tree: Better Buffs.
- Earth Tree: More Tank.
    - Pheonix - If you burn to death, revive from the ashes with X% HP (Once per battle)

## Progression
- Neutral Skills
    - Provided after Tutorial.
- Elemental Skills
    - Get 1 Spendable point per level, can be used in any elemental tree. (borderlands style)
- Element Proficiency.
    - Become more powerful based on usage, e.g. use Fire I ten times to unlock Fire II, 
    Increases effect of skills slightly.
    - As the player becomes more proficient in specific elements, they unlock additional branches within the elements 
    (e.g. blue fire for Fire, Ice for Water, something plant-related for Earth)
- Stat Points 
    - Can spend to increase base stats. Levelling will automatically increase stats.
    - Easter Egg: Additional Passives when all stat points are assigned to a single skill. E.g. Everything in defense. 

## Enemies
- Single Elemental Types.
- Flexible Elemental types (like player can switch, maximum 2 elements?)
- Bosses:
    - One for each Augment Type. (Electric, Light, Dark.)
        - Dark - Adonut. (Spherical, Black hole center).
    - Augment bosses to be able to be done in any order. Decided by a cryptic-ish option box after tutorial. 
    Options: Dark -> Dark Orb First, Light -> Light First, Don't Care -> Electric First. 
    - Big mega final boss.
- Level scalable, to account for exploring in any direction.

## World
- Overworld: 3D Top Down.
- Partitionable - e.g. Fire Area, Water Area ect. This allows for reordering of certain story elements based on actions.
- Each Partition can be made up of any number of "Segments". A Segment is the equivalent of 1 sub area. 
i.e. A Small Town would be a segment. Anything that requires transition, requires a separate segment.
- Battle: 3D Side View
- Opportunities for exploration: Wisp Buddies. Lore Fragments. Special abilities? NPC quests
- Specific levels of element trees needed in dungeons? E.g. Fire III required for a dungeon to unlock. 
Not to the point that all skill trees have to be maxed out, but to the point that the player has dabbled in each of the 
elements a little. Any requirements shouldn't block story.

## Endgame / Replayability
- Hats: Endgame reward, changes neutral tree. (i.e. Attack is replaced with X) 
- Player can restart plotline, while keeping hats already obtained from previous playthroughs to play around with 
alternative playstyles
- Plotline length: Depends on Lore and general pacing. Will probably be decided after playtesting.
- Speed Run Mode:  Adds a Timer to the UI, Skips all dialog & cutscenes. 
- Additional collectibles?

## Technical Design
We'll be using Unreal Engine. See [Tech Design](./TechDesign/TechDesign.md) for more information.

## Artistic Design
General art syle will be a magical / fantasy setting. See [Art Design](./ArtDesign/ArtDesign.md) for more information.
 
## World Design
The Story is currently based on elements and monsters. As such I'm imagining magically rich environments, 
a few examples could be: Luscious Forests (Earth), Fiery Canyons (Fire), Swamp? (Water), Crystal filled caves (Dark), 
Blinding Desert? (Light), Sky Base constantly struck by lightning? (Electric).

There will also be the concept of friendly characters, so some form of civilization will need to be included. 
There's not currently a reason to have a designated town for each area, so a large amount of flexibility with story is 
available.

Areas should be able to be explored in any order OR previous actions will determine the ordering of areas. 
This means the areas will have to have clear partitions. Each overall area may have randomised segments. 
The training area will always be the first area. Each hat might have a different training area? 

#### Full Lore
The wisp is bored. Trains to become a warrior. Wants to get more powerful, so starts chasing down the toughest bosses. 
Which leads to the seeking of the first augment boss.

Defauting all Augment bosses summons final boss. 
The combination of these augment elements lends itself to a crystal that can supposedly be used to gain infinite power. 
Upon using the crystal, the game resets, with the wisp having no knowledge of the previous playthrough. 

After the first boss that is defeated, it's spirit will then talk to you among the rest of the journey, 
influencing your character, as such by defeating the dark boss first your thoughts will become more negative. 
This Splits into 3 lore options.

##### Lore Option 1 (Neutral Wisp) - Lightning First. 
Only really cares about fighting tough enemies. 

##### Lore Option 2 (Evil Wisp) - Dark First.
Unknown to the player, the wisp starts of by helping his fellow elementals, 
eventually as the story progresses the enemies stop being beast based and start being human based. 
This reveals an underlying conflict between humans and elementals. 
As the player progresses the conflict deepens and it reveals the wisp is plotting the destruction of the human race.  

Reasons for the conflict:
- The wisp sees the humans as lesser beings/a disease/resource-greedy/unsustainable
- Humans abusing the elementals, Capturing lightning elementals for power and such.
- Wisp-abuse lore but then later it's revealed that the humans are just trying to defend themselves from the wisps who 
are already attacking humans. So the 'abuse' is the humans trying to reduce the threat the wisps pose 
(e.g. your lightning example is them draining the power of the hostile wisp and generating extra power the humans use 
for defense technology)

##### Lore Option 3 (Good Wisp) - Light First.
Similar to Evil Wisp, however this time, since the humans are encountered earlier the bond between them and the wisp is 
stronger, allowing the conflict to be resolved peacefully.

## Music Design
To be looked at once lore and map are further fleshed out.

## Credits
So I don't forget who's helped.