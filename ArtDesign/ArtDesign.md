# Art Design
This is to provide inspiration and clarity on the artistic designs, where possible concept art should be included
and updated.

## Basic Setting
The basic setting is of a magical / mystical world in which elementals and wild beasts roam. 
There is also a small human settlement, the humans aren't very technologically advanced, mostly reliant on magic.

## The Elements
The general design of the game should link closely to the 6 Elements, these are the founding blocks of the world.
In Addition, each element will need its own icon. I've created some very basic examples below:

![Dark](/ArtDesign/Elements/Element_DARK.png)
![Light](/ArtDesign/Elements/Element_LIGHT.png)
![Earth](/ArtDesign/Elements/Element_EARTH.png)
![Fire](/ArtDesign/Elements/Element_FIRE.png)
![Water](/ArtDesign/Elements/Element_WATER.png)
![Electric](/ArtDesign/Elements/Element_ELECTRIC.png)
![Neutral](/ArtDesign/Elements/Element_NEUTRAL.png)  

These will be displayed in the battle screen so will need improvement and animations.

## The Player
The Player is a wisp, this means he's a very ethereal and floaty being, with no fixed form. 
In the tutorial he will start of as neutral. In addition, he can harness the power of the 3 primary elements.
This means he will have a minimum of 4 forms. 
- Neutral Form
- Fire Form
- Earth Form
- Water Form

It's optional as if to include augment elements. If Augments are included they should be minor effects.
For Example, small sparks for Electric. This would lead to 10 Forms.
- Neutral Form
- Fire Form, With Light Augment
- Fire Form, With Dark Augment
- Fire Form, With Electric Augment
- Earth Form, With Light Augment
- Earth Form, With Dark Augment
- Earth Form, With Electric Augment
- Water Form, With Light Augment
- Water Form, With Dark Augment
- Water Form, With Electric Augment 

There's a few directions we can go with this, I've found some images on the internet as examples.
#### Orb
![Orb](/ArtDesign/PlayerWisp/OrbStyle.jpg)
#### Spirit
![Spirit](/ArtDesign/PlayerWisp/Spirit.jpg)
#### Humanoid
![Humanoid](/ArtDesign/PlayerWisp/Humanoid.jpg)

## The World
The world splits into 10 Segments.
- Tutorial Area. This Area has yet to be thematically designed. 
- Caves. The Location of the Dark Boss.
- Desert. The Location of the Light Boss.
- Clouds. The Location of the Electric Boss.
- Forest. The Earth Area Adjacent to Tutorial Area.
- Lake. The Water Area Adjacent to the Tutorial Area.
- Volcano. The Fire Area Adjacent to the Tutorial Area.
- Valley. The Area Between Forest and Volcano.
- Mountain. The Area Between Lake and Volcano.
- Swamp. The Area Between Forest and Lake.

A Very basic map is shown below:
![Map](/ArtDesign/World/BasicMap.png)
I've talked to someone about a more detailed (but still prototype) version of this map.

#### Tutorial Area
This area is the first area the player will see, it connects to the 3 primary element areas: Forest, Lake and Volcano.

Since it is the tutorial and hub area, I imagine some form of wisp/elemental settlement. 
However, I'm unsure how that would look thematically. 
 
#### Caves
This is the location of the dark boss, as such it will a complex cave system with the boss at the deepest part.
We can't have the entire cave be pitch black, so it will need some form of dim light for player visibility. 
Lightly glowing purple crystals might be the best fit thematically. 

#### Desert
This is the location of the light boss, as such the area will be a very large and bright desert. 
The problem with an expansive desert is there's generally nothing in them. We need to keep the thematic while not
having everything so spaced out it's dull or tedious. The desert is the closest biome to the human settlement,
we might be able to use this by placing various outposts in the desert.   

#### Clouds
This is the location of the electric boss, as such the area should be under regular thunder strikes. 
This area is also the most difficult to get to, need to be shot out of a volcano, so needs to represent that harshness.
The biggest problem is what form this area should take, are they just clouds or some other structure?

#### Forest
As one of the first areas the player can enter, it should be a very simplistic area with minimal enemy variety.
As the forest extends closer to the caves, the vegetation should become very thick, with minimal light filtering
through the tree tops. 

#### Lake
Similar to the forest, a simplistic area with minimal variety. The closer to the desert, the more the lake becomes a 
beach. The difficulty with this area will be making the lake interesting, perhaps with some small islands in it.

#### Volcano
This might be the most complicated to design biome, with the top of the volcano itself being used as a method to get 
to the cloud area, this area will have a largely vertical design. Might be worth adding in some platforming elements?

#### Valley
A Transitional area between the volcano and the forest. There doesn't have to be anything special going on here.
Since it's a transitional area, it should have a good variety of enemies. The fact it is a valley might also lend it
to some platforming elements. 

#### Mountain
A Transitional area between the volcano and the lake. Yet another vertical based map. 
Something interesting to note here is that this is the source of the lake. 
It would be cool if you could ride the river down to the lake area.

#### Swamp
A Transitional area between the forest and the lake. Make it swampy.

## Enemies