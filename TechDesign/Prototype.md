# Prototype Design
This details the minimum requirements for a fully functioning prototype. 

## Overworld
Obviously one of the first things that needs to be done is the overworld, the prototype will attempt to create the tutorial area. 
Since it's the tutorial area this means the space will probably be rather linear. 

##### Functions required for overworld:
- Movement of player Character.
- Collision with World Objects.
- Out of Bounds Prevention.
- Interaction with NPCs. (Display Text)
- Interaction with Enemies. (Trigger Battle)
- Transition to another area.

## Battles
The second thing that needs to be done. Interactions with Enemies in the overworld will trigger a battle.
We will also almost certianly want a simulator to test various balance aspects.

##### Functions required for battles:
- Basic Battle Mechanics. (Attack, Defend) 
- Enemy AI.
- Player & Enemy Stats.
- Element Switching in Combat.
- Death & Rewards.

## UI
- Main Menu.
- Saving & Loading.
- Options.

## Misc
- Controller Inputs.
- Audio.