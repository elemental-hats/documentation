# Technical Design
This is for all things technical, taking the mechanics defined in the mechanics document and converting them into 
code based structures as well as defining how they relate and what properties they have.

## Engine
I've decided that Unreal is the engine of choice. At first, Java with libGDX was chosen, mainly for comfort reasons. 
However, with the addition of Mav & Stu, we can move to a more complicated and refined Engine that is better for 3D.
The blueprint system also allows for fast, although inefficient, development even without programming knowledge.
This will enable Mav to have the flexibility to update animations and models inside engine.

## Example Class